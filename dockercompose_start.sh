#!/bin/sh

set -e

export DOCKER_IMAGE=platformbuilders-client-api-service

echo "Removing Unwanted Containers"
docker rm -v $(docker ps -a -q -f "status=exited") &
echo "[DONE] Removing Unwanted Containers"

echo "Removing Unwanted Images"
docker rmi $(docker images -q -f "dangling=true") 2>/dev/null &

echo "Removing Unwanted Volumes"
docker volume rm $(docker volume ls -q -f "dangling=true") 2>/dev/null &
docker run -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/docker:/varlib/docker --rm martin/docker-cleanup-volumes &
echo "[DONE] Removing Unwanted Volumes"

echo "Building Docker Compose Images ..."
docker-compose build --no-cache
echo "[DONE] Building Docker Compose Images  ..."

echo "Starting Docker Compose Containers..."
docker-compose up --exit-code-from ${DOCKER_IMAGE}
echo "[DONE] Starting Docker Compose Containers..."

