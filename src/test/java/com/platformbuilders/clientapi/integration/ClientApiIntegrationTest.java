package com.platformbuilders.clientapi.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.platformbuilders.clientapi.dto.ClientDTO;
import com.platformbuilders.clientapi.exception.NoDataFoundException;
import com.platformbuilders.clientapi.service.ClientService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Locale;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(
        locations = {"classpath:application.properties"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
public class ClientApiIntegrationTest {
    private final ClientDTO clientDTO = ClientDTO.builder().name("Wagner Test").birthdayDate(LocalDate.of(1983, 04, 01)).cpf("15688337095").build();
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private MockMvc mvc;

    @Autowired
    private ClientService clientService;

    private static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void givenAPersistedClientWhenCallGETShouldReturnData() throws Exception {

        clientService.createClient(clientDTO);

        MvcResult result = mvc.perform(
                get("/v1/clientes").contentType(MediaType.APPLICATION_JSON).header("apikey", "zQckkrGbqkPLAqspNeh71eAvXrDnmhA3")
        ).andReturn();

        assertNotNull(result);
        assertEquals(result.getResponse().getStatus(), 200);
        assertTrue(result.getResponse().getContentAsString().contains("{\"content\":[{\"links\":[],\"id\":1,\"nome\":\"Wagner Test\",\"cpf\":\"156.883.370-95\",\"dataNascimento\":\"1983-04-01\",\"idade\":37}],\"pageable\":{\"sort"));
    }

    @Test
    public void givenNonePersistedClientWhenCallGETShouldReturnNoContentData() throws Exception {

        MvcResult result = mvc.perform(
                get("/v1/clientes").contentType(MediaType.APPLICATION_JSON).header("apikey", "zQckkrGbqkPLAqspNeh71eAvXrDnmhA3")
        ).andReturn();

        assertNotNull(result);
        assertEquals(result.getResponse().getStatus(), 204);
        assertEquals(result.getResponse().getContentAsString(), "{\"message\":\"" + messageSource.getMessage("client.crud.thereIsNoSufficientData", null, new Locale("en")) + "\"}");
    }

    @Test
    public void givenClientWhenCallPOSTShouldReturnCreatedEntity() throws Exception {

        MvcResult result = mvc.perform(
                post("/v1/clientes").content(asJsonString(clientDTO)).contentType(MediaType.APPLICATION_JSON).header("apikey", "zQckkrGbqkPLAqspNeh71eAvXrDnmhA3")
        ).andReturn();

        assertNotNull(result);
        assertEquals(result.getResponse().getStatus(), 201);
        assertEquals(result.getResponse().getContentAsString(), "{\"_links\":{\"self\":{\"href\":\"http://localhost/v1/clientes/1\"}},\"id\":1,\"nome\":\"Wagner Test\",\"cpf\":\"156.883.370-95\",\"dataNascimento\":\"1983-04-01\",\"idade\":37}");
    }

    @Test
    public void givenClientWithSameCPFFromAnotherPersistedClientWhenCallPOSTShouldReturnError() throws Exception {

        Optional<ClientDTO> createdClientDTO = clientService.createClient(clientDTO);
        assertTrue(createdClientDTO.isPresent());
        assertEquals(createdClientDTO.get().getId(), 1L);
        assertEquals(createdClientDTO.get().getName(), "Wagner Test");

        MvcResult result = mvc.perform(
                post("/v1/clientes").content(asJsonString(clientDTO)).contentType(MediaType.APPLICATION_JSON)
                        .header("apikey", "zQckkrGbqkPLAqspNeh71eAvXrDnmhA3")
                        .header("Accept-Language", "pt")
        ).andReturn();

        assertNotNull(result);
        assertEquals(result.getResponse().getStatus(), 409);
        assertEquals(result.getResponse().getContentAsString(), "{\"message\":\"" + messageSource.getMessage("client.crud.alreadyExists", null, new Locale("pt")) + "15688337095\"}");
    }

    @Test
     public void givenExistentClientWhenCallPUTShouldReturnUpdatedEntity() throws Exception {

        Optional<ClientDTO> createdClientDTO = clientService.createClient(clientDTO);

        assertTrue(createdClientDTO.isPresent());
        assertEquals(createdClientDTO.get().getId(), 1L);
        assertEquals(createdClientDTO.get().getName(), "Wagner Test");


        ClientDTO existingClientDTO = createdClientDTO.get();
        existingClientDTO.setName("Wagner Domingos");

        MvcResult result = mvc.perform(
                put("/v1/clientes").content(asJsonString(existingClientDTO)).contentType(MediaType.APPLICATION_JSON).header("apikey", "zQckkrGbqkPLAqspNeh71eAvXrDnmhA3")
        ).andReturn();

        assertNotNull(result);
        assertEquals(result.getResponse().getStatus(), 200);
        assertEquals(result.getResponse().getContentAsString(), "{\"_links\":{\"self\":{\"href\":\"http://localhost/v1/clientes/1\"}},\"id\":1,\"nome\":\"Wagner Domingos\",\"cpf\":\"156.883.370-95\",\"dataNascimento\":\"1983-04-01\",\"idade\":37}");
    }


    @Test
     public void givenExistentClientWhenCallDELETEShouldReturnNoContent() throws Exception {

        Optional<ClientDTO> createdClientDTO = clientService.createClient(clientDTO);

        assertTrue(createdClientDTO.isPresent());
        assertEquals(createdClientDTO.get().getId(), 1L);
        assertEquals(createdClientDTO.get().getName(), "Wagner Test");


        MvcResult result = mvc.perform(
                delete("/v1/clientes/" + createdClientDTO.get().getId()).contentType(MediaType.APPLICATION_JSON).header("apikey", "zQckkrGbqkPLAqspNeh71eAvXrDnmhA3")
        ).andReturn();

        assertNotNull(result);
        assertEquals(result.getResponse().getStatus(), 204);

        Assertions.assertThrows(NoDataFoundException.class, () -> {
            clientService.findById(createdClientDTO.get().getId());
        });
    }
}
