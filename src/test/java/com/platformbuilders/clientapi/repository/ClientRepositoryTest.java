package com.platformbuilders.clientapi.repository;

import com.platformbuilders.clientapi.model.Client;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Optional;

import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;
import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertFalse;
import static org.springframework.test.util.AssertionErrors.assertNotNull;
import static org.springframework.test.util.AssertionErrors.assertTrue;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD,
        classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@Transactional
public class ClientRepositoryTest {
    @Autowired
    private ClientRepository repository;
    private Client client;

    @BeforeEach
    public void setup() {
        repository.deleteAll();
        this.client = new Client(null, "Wagner Test", "15688337095", LocalDate.of(1983, 04, 01));
    }

    @Test
    public void givenProperClientWhenCallSaveShouldPersistEntity() {
        Client clientPersisted = this.repository.save(client);
        assertNotNull("Client", clientPersisted);
        assertNotNull("ID", clientPersisted.getId());
        assertEquals("CPF", clientPersisted.getCpf(), "15688337095");
        assertEquals("Name", clientPersisted.getName(), "Wagner Test");
        assertEquals("Birthday", clientPersisted.getBirthdayDate(), LocalDate.of(1983, 04, 01));
    }

    @Test
    public void givenExistingDataWhenCallDeleteShouldRemoveData() {
        Client clientPersisted = this.repository.save(client);
        repository.deleteById(clientPersisted.getId());
        assertFalse("Null client by Id", this.repository.findById(clientPersisted.getId()).isPresent());
        assertFalse("Null client by CPF", this.repository.findByCpf("15688337095").isPresent());
    }

    @Test
    public void givenPersistedClientWhenCallUpdateShouldUpdateTheEntity() {
        Client clientPersisted = this.repository.save(client);
        clientPersisted.setName("Wagner Domingos Test");
        Client clientUpdated = this.repository.save(clientPersisted);

        assertEquals("CPF", clientUpdated.getCpf(), "15688337095");
        assertEquals("Name", clientUpdated.getName(), "Wagner Domingos Test");

    }

    @Test
    public void givenPersistedClientWhenTryToSaveAnotherWithSameInformationShouldThrowException() {
        this.repository.save(client);
        Client newClientSameData = new Client(null, "Wagner Test", "15688337095", LocalDate.of(1983, 04, 01));

        Assertions.assertThrows(Exception.class, () -> {
            this.repository.save(newClientSameData);
        });
    }

    @Test
    public void givenExistingDataWhenFindByNameIgnoreCaseContainingShouldReturnData() {
        Client clientPersisted = this.repository.save(client);
        Page<Client> fetchedClients = this.repository.findByNameIgnoreCaseContaining(null, "Wagner");
        assertNotNull("Client", fetchedClients);
        assertEquals("Total Elements", fetchedClients.getTotalElements(), 1L);
    }

    @Test
    public void givenExistingDataWhenFindByCpfShouldReturnData() {
        Client clientPersisted = this.repository.save(client);
        Optional<Client> fetchedClient = this.repository.findByCpf("15688337095");
        assertTrue("Client", fetchedClient.isPresent());
        assertEquals("CPF", fetchedClient.get().getCpf(), "15688337095");
    }


}
