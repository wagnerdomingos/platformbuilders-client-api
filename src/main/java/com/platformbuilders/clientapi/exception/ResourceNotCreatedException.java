package com.platformbuilders.clientapi.exception;

public class ResourceNotCreatedException extends RuntimeException {
    public ResourceNotCreatedException(String msg) {
        super(msg);
    }
}
