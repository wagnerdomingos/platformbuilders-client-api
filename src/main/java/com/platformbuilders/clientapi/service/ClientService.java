package com.platformbuilders.clientapi.service;

import com.platformbuilders.clientapi.dto.ClientDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface ClientService {

    Page<ClientDTO> findAll(Pageable pageable);

    Optional<ClientDTO> findById(Long id);

    Optional<ClientDTO> findByCpf(String cpf);

    Page<ClientDTO> findByName(Pageable pageable, String name);

    Optional<ClientDTO> createClient(ClientDTO clientDTO);

    Optional<ClientDTO> updateClient(ClientDTO clientDTO);

    void deleteById(Long id);

    void eraseAllData();
}
