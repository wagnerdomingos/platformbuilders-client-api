package com.platformbuilders.clientapi.service;

import com.platformbuilders.clientapi.dto.ClientDTO;
import com.platformbuilders.clientapi.exception.NoDataFoundException;
import com.platformbuilders.clientapi.exception.ResourceAlreadyExistsException;
import com.platformbuilders.clientapi.exception.ResourceNotCreatedException;
import com.platformbuilders.clientapi.model.Client;
import com.platformbuilders.clientapi.repository.ClientRepository;
import com.platformbuilders.clientapi.util.CPFUtil;
import com.platformbuilders.clientapi.util.ClientTranslator;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ClientServiceImpl implements ClientService {

    Logger logger = LoggerFactory.getLogger(ClientServiceImpl.class);

    ClientRepository repository;
    MessageSource messageSource;


    public ClientServiceImpl(ClientRepository repository, MessageSource messageSource) {
        this.repository = repository;
        this.messageSource = messageSource;
    }

    @Override
    public Page<ClientDTO> findAll(Pageable pageable) {
        logger.info("ClientService :: findall ");
        Page<Client> clients = repository.findAll(pageable);
        if (clients.isEmpty()) {
            throw new NoDataFoundException(messageSource.getMessage("client.crud.thereIsNoSufficientData", null, LocaleContextHolder.getLocale()));
        }
        int totalElements = (int) clients.getTotalElements();
        return new PageImpl<ClientDTO>(clients
                .stream()
                .map(client -> ClientTranslator.to(client))
                .collect(Collectors.toList()), pageable, totalElements);
    }

    @Override
    public Optional<ClientDTO> findById(Long id) {
        logger.info("ClientService :: findbyID ::  id: " + id);

        Optional<Client> client = repository.findById(id);
        if (!client.isPresent()) {
            throw new NoDataFoundException(messageSource.getMessage("client.crud.notExistsClientWithId", null, LocaleContextHolder.getLocale()) + id);
        }
        return Optional.of(ClientTranslator.to(client.get()));
    }

    @Override
    public Page<ClientDTO> findByName(Pageable pageable, String name) {
        logger.info("ClientService :: findByName :: name: " + name);

        Page<Client> clients = repository.findByNameIgnoreCaseContaining(pageable, name);
        if (clients.isEmpty()) {
            throw new NoDataFoundException(messageSource.getMessage("client.crud.thereIsNoSufficientDataSearchByName", null, LocaleContextHolder.getLocale()) + name);
        }
        int totalElements = (int) clients.getTotalElements();
        return new PageImpl<ClientDTO>(clients
                .stream()
                .map(client -> ClientTranslator.to(client))
                .collect(Collectors.toList()), pageable, totalElements);
    }

    @Override
    public Optional<ClientDTO> findByCpf(String cpf) {
        logger.info("ClientService :: findByCpf :: cpf: " + cpf);

        Optional<Client> client = repository.findByCpf(CPFUtil.unformatCPF(cpf));
        if (client.isEmpty()) {
            throw new NoDataFoundException(messageSource.getMessage("client.crud.notExistsClientWithCPF", null, LocaleContextHolder.getLocale()) + CPFUtil.formatCpf(cpf));
        }
        return Optional.of(ClientTranslator.to(client.get()));
    }

    @Override
    public Optional<ClientDTO> createClient(ClientDTO clientDTO) {
        logger.info("ClientService :: createClient :: client: " + clientDTO.toString());

        verifyIfCPFAlreadyRegistered(clientDTO);
        verifyIfIsValidBirthdayDate(clientDTO);
        Client client = repository.save(ClientTranslator.from(clientDTO));
        if (client == null) {
            throw new ResourceNotCreatedException(messageSource.getMessage("client.crud.creationError", null, LocaleContextHolder.getLocale()) + clientDTO.getName());
        }
        return Optional.of(ClientTranslator.to(client));
    }

    @Override
    public Optional<ClientDTO> updateClient(ClientDTO clientDTO) {
        logger.info("ClientService :: updateClient :: client: " + clientDTO.toString());

        Optional<Client> clientBD = repository.findById(clientDTO.getId());
        if (!clientBD.isPresent()) {
            throw new NoDataFoundException(messageSource.getMessage("client.crud.notExistsClientWithId", null, LocaleContextHolder.getLocale())  + clientDTO.getId());
        }
        Client client = clientBD.get();
        //Verify if cpf changed should check if already exists other register with same
        verifyIfCPFAlreadyRegistered(clientDTO, client);

        if (clientDTO.getBirthdayDate() != null) {
            verifyIfIsValidBirthdayDate(clientDTO);
            client.setBirthdayDate(clientDTO.getBirthdayDate());
        }

        if (!Strings.isEmpty(clientDTO.getCpf())) {
            client.setCpf(clientDTO.getCpf());
        }

        if (!Strings.isEmpty(clientDTO.getName())) {
            client.setName(clientDTO.getName());
        }

        return Optional.of(ClientTranslator.to(repository.save(client)));
    }

    @Override
    public void deleteById(Long id) {
        logger.info("ClientService :: deleteById :: id: " + id);
        Optional<Client> client = repository.findById(id);
        if (!client.isPresent()) {
            throw new NoDataFoundException(messageSource.getMessage("client.crud.notExistsClientWithId", null, LocaleContextHolder.getLocale()) + id);
        }
        repository.deleteById(id);
    }

    @Override
    public void eraseAllData() {
        logger.info("ClientService :: eraseAllData ");
        repository.deleteAll();
    }

    private void verifyIfCPFAlreadyRegistered(ClientDTO clientDTO, Client client) {
        String clientDTOUnformattedCPF = CPFUtil.unformatCPF(clientDTO.getCpf());
        if (!client.getCpf().equals(clientDTOUnformattedCPF)) {
            Optional<Client> verifiedClient = repository.findByCpf(clientDTOUnformattedCPF);
            if (verifiedClient.isPresent()) {
                throw new ResourceAlreadyExistsException(messageSource.getMessage("client.crud.alreadyExists", null, LocaleContextHolder.getLocale())  + clientDTO.getCpf());
            }
        }
    }

    private void verifyIfCPFAlreadyRegistered(ClientDTO clientDTO) {
        String clientDTOUnformattedCPF = CPFUtil.unformatCPF(clientDTO.getCpf());
        Optional<Client> verifiedClient = repository.findByCpf(clientDTOUnformattedCPF);
        if (verifiedClient.isPresent()) {
            throw new ResourceAlreadyExistsException(messageSource.getMessage("client.crud.alreadyExists", null, LocaleContextHolder.getLocale()) + clientDTO.getCpf());
        }

    }

    private void verifyIfIsValidBirthdayDate(ClientDTO clientDTO) {
        if (clientDTO.getBirthdayDate().isAfter(LocalDate.now())) {
            throw new ResourceNotCreatedException(messageSource.getMessage("client.crud.notAllowedFutureBirthDate", null, LocaleContextHolder.getLocale()) + clientDTO.getBirthdayDate());
        }

    }

}
