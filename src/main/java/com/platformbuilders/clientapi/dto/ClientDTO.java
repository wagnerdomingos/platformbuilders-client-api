package com.platformbuilders.clientapi.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.platformbuilders.clientapi.util.DateSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.RepresentationModel;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;


@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ClientDTO extends RepresentationModel<ClientDTO> {
    @JsonProperty("id")
    private Long id;

    @NotEmpty(message = "{client.name.notEmpty}")
    @JsonProperty("nome")
    private String name;

    @NotEmpty(message = "{client.cpf.notEmpty}")
    @CPF
    @JsonProperty("cpf")
    private String cpf;

    @NotNull(message = "{client.birthdate.notEmpty}")
    @JsonProperty("dataNascimento")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonSerialize(using = DateSerializer.class)
    private LocalDate birthdayDate;

    @JsonProperty("idade")
    private Integer age;

}
