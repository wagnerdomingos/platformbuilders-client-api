package com.platformbuilders.clientapi.advice;

import com.platformbuilders.clientapi.exception.NoDataFoundException;
import com.platformbuilders.clientapi.exception.ResourceAlreadyExistsException;
import com.platformbuilders.clientapi.exception.ResourceNotCreatedException;
import com.platformbuilders.clientapi.message.ResponseMessage;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class ExceptionAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(NoDataFoundException.class)
    public ResponseEntity<ResponseMessage> handleNoDataFoundException(NoDataFoundException exc) {
        logger.error(exc.getMessage());
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new ResponseMessage(exc.getMessage()));
    }

    @ExceptionHandler(ResourceNotCreatedException.class)
    public ResponseEntity<ResponseMessage> handleResourceNotCreatedException(ResourceNotCreatedException exc) {
        logger.error(exc.getMessage());
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new ResponseMessage(exc.getMessage()));
    }

    @ExceptionHandler(ResourceAlreadyExistsException.class)
    public ResponseEntity<ResponseMessage> handleResourceAlreadyExistsException(ResourceAlreadyExistsException exc) {
        logger.error(exc.getMessage());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ResponseMessage(exc.getMessage()));
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ResponseMessage> handleConstraintViolationExceptionException(ConstraintViolationException exc) {
        logger.error(exc.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(exc.getMessage()));
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exc,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        String errorMessage = exc.getBindingResult().getFieldErrors().get(0).getDefaultMessage();
        logger.error(errorMessage);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(errorMessage));
    }

}
