package com.platformbuilders.clientapi.model;

import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "client")
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotEmpty(message = "{client.name.notEmpty}")
    @Column(name = "name", nullable = false)
    private String name;

    @NotEmpty(message = "{client.cpf.notEmpty}")
    @CPF(message = "{client.cpf.invalid}")
    @Column(name = "cpf", nullable = false, unique = true)
    private String cpf;

    @NotNull(message = "{client.birthdate.notEmpty}")
    @Column(name = "birthdayDate", nullable = false)
    private LocalDate birthdayDate;

    public Client() {
    }

    public Client(Long id, String name, String cpf, LocalDate birthdayDate) {
        this.id = id;
        this.name = name;
        this.cpf = cpf;
        this.birthdayDate = birthdayDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;
        Client client = (Client) o;
        return id.equals(client.id) &&
                name.equals(client.name) &&
                cpf.equals(client.cpf) &&
                birthdayDate.equals(client.birthdayDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, cpf, birthdayDate);
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", cpf='" + cpf + '\'' +
                ", birthdayDate=" + birthdayDate +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getBirthdayDate() {
        return birthdayDate;
    }

    public void setBirthdayDate(LocalDate birthdayDate) {
        this.birthdayDate = birthdayDate;
    }
}
