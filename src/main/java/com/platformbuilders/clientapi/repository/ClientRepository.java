package com.platformbuilders.clientapi.repository;

import com.platformbuilders.clientapi.model.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client, Long> {

    Logger logger = LoggerFactory.getLogger(ClientRepository.class);

    Page<Client> findByNameIgnoreCaseContaining(Pageable pageable, String name);

    Optional<Client> findByCpf(String cpf);
}
