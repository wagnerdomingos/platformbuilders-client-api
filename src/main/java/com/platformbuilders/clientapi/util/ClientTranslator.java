package com.platformbuilders.clientapi.util;

import com.platformbuilders.clientapi.dto.ClientDTO;
import com.platformbuilders.clientapi.model.Client;

public class ClientTranslator {
    public static ClientDTO to(Client client) {
        return ClientDTO.builder()
                .age(AgeCalculator.calculateAge(client.getBirthdayDate()))
                .birthdayDate(client.getBirthdayDate())
                .cpf(CPFUtil.formatCpf(client.getCpf()))
                .id(client.getId())
                .name(client.getName())
                .build();
    }

    public static Client from(ClientDTO clientDTO) {
        return new Client(clientDTO.getId(),
                clientDTO.getName(),
                CPFUtil.unformatCPF(clientDTO.getCpf()),
                clientDTO.getBirthdayDate());

    }


}
