package com.platformbuilders.clientapi.util;


import javax.swing.text.MaskFormatter;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CPFUtil {
    public static String formatCpf(String cpf) {
        try {
            MaskFormatter mask = new MaskFormatter("###.###.###-##");
            mask.setValueContainsLiteralCharacters(false);
            return mask.valueToString(cpf);
        } catch (ParseException ex) {
            Logger.getLogger(CPFUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return cpf;
    }

    public static String unformatCPF(String cpf) {
        if (cpf != null) {
            String unformattedCpf = cpf.replace(".", "");
            unformattedCpf = unformattedCpf.replace("-", "");
            return unformattedCpf;
        }
        return null;
    }
}
