package com.platformbuilders.clientapi.controller;

import com.platformbuilders.clientapi.dto.ClientDTO;
import com.platformbuilders.clientapi.service.ClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Optional;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@CrossOrigin("http://localhost:8080")
@RestController
@RequestMapping(path = "/v1/clientes", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
public class ClientController {
    Logger logger = LoggerFactory.getLogger(ClientController.class);

    ClientService clientService;

    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping
    @Cacheable(value = "allClientsCache")
    public ResponseEntity<?> getAllClients(@PageableDefault(page = 0,
            size = 3,
            sort = "name",
            direction = Sort.Direction.ASC) Pageable pageable) {
        logger.info("Executing method :: getAllClients");
        Page<ClientDTO> clients = clientService.findAll(pageable);
        return new ResponseEntity<>(clients, HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<?> getClientById(@PathVariable("id") Long id) {
        logger.info("Executing method :: getClientById");
        Optional<ClientDTO> clientDTO = clientService.findById(id);
        return new ResponseEntity<>(clientDTO.get(), HttpStatus.OK);
    }

    @GetMapping(path = "/cpf/{cpf}")
    public ResponseEntity<?> getClientByCPF(@PathVariable("cpf") String cpf) {
        logger.info("Executing method :: getClientByCPF");
        Optional<ClientDTO> clientDTO = clientService.findByCpf(cpf);
        return new ResponseEntity<>(clientDTO.get(), HttpStatus.OK);
    }

    @GetMapping(path = "/name/{name}")
    public ResponseEntity<?> getClientsByName(@PageableDefault(page = 0,
            size = 3,
            sort = "name",
            direction = Sort.Direction.ASC) Pageable pageable, @PathVariable("name") String name) {
        logger.info("Executing method :: getClientsByName");
        Page<ClientDTO> clients = clientService.findByName(pageable, name);
        return new ResponseEntity<>(clients, HttpStatus.OK);
    }

    @PostMapping
    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(value = "allClientsCache", allEntries = true)
    public ResponseEntity<?> createClient(@Valid @RequestBody ClientDTO clientDTO) {
        Optional<ClientDTO> client = clientService.createClient(clientDTO);
        ClientDTO clientResponse = client.get();
        clientResponse.add(linkTo(ClientController.class).slash(clientResponse.getId()).withSelfRel());
        return new ResponseEntity<>(clientResponse, HttpStatus.CREATED);
    }

    @PutMapping
    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(value = "allClientsCache", allEntries = true)
    public ResponseEntity<?> updateCliente(@Valid @RequestBody ClientDTO clientDTO) {
        Optional<ClientDTO> client = clientService.updateClient(clientDTO);
        ClientDTO clientResponse = client.get();
        clientResponse.add(linkTo(ClientController.class).slash(clientResponse.getId()).withSelfRel());
        return new ResponseEntity<>(clientResponse, HttpStatus.OK);
    }

    @PatchMapping
    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(value = "allClientsCache", allEntries = true)
    public ResponseEntity<?> changeClient(@RequestBody ClientDTO clientDTO) {
        Optional<ClientDTO> client = clientService.updateClient(clientDTO);
        ClientDTO clientResponse = client.get();
        clientResponse.add(linkTo(ClientController.class).slash(client.get().getId()).withSelfRel());
        return new ResponseEntity<>(clientResponse, HttpStatus.OK);
    }

    @DeleteMapping(path = "/{id}")
    @CacheEvict(value = "allClientsCache", allEntries = true)
    public ResponseEntity<?> deleteClient(@PathVariable("id") Long id) {
        clientService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
