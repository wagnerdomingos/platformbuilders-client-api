FROM gradle:6.6.1-jdk11 AS build-stage
COPY build.gradle /build/
COPY settings.gradle /build/
COPY gradle /build/gradle/
COPY src /build/src/
WORKDIR /build/
RUN gradle bootJar

FROM adoptopenjdk/openjdk11:jdk-11.0.6_10-alpine-slim AS prod-stage
WORKDIR /app
COPY --from=build-stage /build/build/libs/platformbuilders-clientapi-0.0.1-SNAPSHOT.jar /app/
ENTRYPOINT ["java", "-jar", "platformbuilders-clientapi-0.0.1-SNAPSHOT.jar"]