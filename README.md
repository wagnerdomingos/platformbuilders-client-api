# Platform Builder Client API - Reference Guide (by Wagner Domingos da Silva Santos)

## Project Description
This project was built to serve as a CRUD REST API for a Client entity. 

---
## About the Author
Wagner Domingos, former senior software consultant at Avenue Code (clients eBay/GAP), currently working at Numbrs as Senior Backend Engineer with more than 15 years of experience and knowledge about software analysis, development and architecture, involving in particular Java language and related frameworks, acting also with coaching and leadership (technical and behavioral) of development teams using agile methodologies.
Solid business knowledge about Aerospacial Program, Commerce, Financial Investments Management, Fiscal/Billing ( CTe issue, taxation IFRS), Capitalization, GED, Phone Billing, Asset/Equipment Management and Maintenance, Digital Products/Marketing, Life Insurance and some other business areas related to the main segments of country economy like: mining, railway, port, oil and gas, fertilizer, bank, telephony, energy, portage, digital marketing and BSS Telecom. 

* [Linkedin] https://www.linkedin.com/in/wagner-domingos-da-silva-santos-981278100/
* [Email] wagnerdomingos@gmail.com
---

## Project Structure

``` bash
├── clientapi - Project Containing all the sources from the project
│   └── src\
│      └── main\
│          ├── java\ 
│          └── resources\
│      └── test\
│          ├── java\ 
│          └── resources\
├── build.gradle
├── gradle
│   └── wrapper
├── properties.gradle
├── gradlew
├── gradlew.bat
└── settings.gradle	
```

---

### Internal Architecture
The **Client API** itself has a pretty much common internal architecture:

  * `Controller` class that provides _REST_ endpoints and deal with _HTTP_ requests and responses
  * `Service` class that contains some business rules, and abstract the communication between Controller and Repository
  * `Repository` interface with the _database_ (MySQL) and take care of writing and reading data to/from persistent storage

``` bash

  Request  ┌──────── ClientAPI  SpringBoot Rest API ───────────────┐
   ─────────→ ┌─────────────┐    ┌─────────────┐   ┌─────────────┐ │   ┌─────────────┐
   ←───────── │  Controller │ ←→ │   Service   │ ←→│  Repository │←──→ │  Database   │
  Response │  └─────────────┘    └─────────────┘   └─────────────┘ │   └─────────────┘
           │                                                       │
           └─────────-─────────────────────────────────────────────┘
    
```  

---

### Running the application
[Note:] You must need to have in place an existing instance of MySQL listening on port 3306.

* You can run it as a Java application on IntelliJ itself by executing ClientAPIApplication's main method


####  MySQL on Docker for local development

```bash
$ docker container run --name mysqldb-service -ti -p 3306:3306 -e MYSQL_ROOT_HOST=% -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=clientdb -d mysql
```
* You can run it using gradlew’s support, by executing:
``` bash
./gradlew clean build 
./gradlew bootRun
```

After that, the application should be available on http://localhost:8080
---
#### Everything on Docker with docker-compose

```bash
For starting
$ docker-compose up

# For shutting down
$ docker-compose down
```
---
[Note(2):] In the case you really need to dropp of your docker images, and start it from scratch, there is a script in place to provide this mechanism:
#### Starting Docker from scratch (Removing images and rebuilding all those)
``` bash
./dockercompose_start.sh
```


### Available Endpoints for v1

#### Headers
All messages should ship those headers from client perspective:
Content-Type: application/json


#### Security
In order to provide a minimal protection to this API, an Api Key mechanism was placed.
So, in each meassage header we should ship this pair:
apikey: zQckkrGbqkPLAqspNeh71eAvXrDnmhA3

### I18n Mechanism
There is an Internacionalization (I18n) mechanism in place. The application allows translate for messages in two
different languages
- English (en)
- Portuguese (pt)

To provide this working good, a new header attribute must be placed in each request to the API
- Accept-Language ( en | pt )

##### [POST] /v1/clientes 
- Performs the Creation of the client. This should be sent as a POST http verb informing a body as form-data, where the key is "id" and will be automatically generated and cpf value should be unique.

##### [GET] /v1/clientes?page=XXX&size=XXX
- Performs the full search into the database gathering all data and returning it paginated. 
To achieve pagination, must provide page and size arguments like:
[http://localhost:8080/v1/clientes?page=0&size=2]

##### [GET] /v1/clientes/{id}
- Performs the search into the database gathering specific Client by his id

##### [GET] /v1/clientes/cpf/{cpf}
- Performs the search into the database gathering specific Client by his cpf

##### [GET] /v1/clientes/name/{nome}}?page=XXX&size=XXX
- Performs the search into the database gathering all Clients that matches the name search criteria 

##### [PUT] /v1/clientes
- Performs the Update of the existing Client over the database with the needed to send the entire payload. 
Id is mandatory in the payload.

##### [PATCH] /v1/clientes
- Performs the Update of the existing Client over the database without need to send the entire payload. 
Id is mandatory in the payload.
##### [DELETE] /v1/clientes/{id}
- Performs the BI over the database gathering which account has the 3rd highest average transaction amount

-- All those endpoints can be verified at swagger page:
[http://localhost:8080/v2/api-docs/] 
Also reachable by rich UI
[http://localhost:8080/swagger-ui.html]

[Note:] For complete appreciation, a postman collection will be available at root folder named as: *Platformbuilders Client API.postman_collection.json*

### Sample Payload for Client Creation
``` json
{
    "nome" : "Wagner Domingos",
    "cpf" : "08198537040",
    "dataNascimento" : "1983-04-01"
}
```
---
### Testing
We have a class in place that performs all E2E testing (integration tests) and repository (DataJPATests)
You can reach those classes at these locations:

``` bash
├── clientAPI - 
│      └── test\
│          ├── java\ 
│             ├── com.platformbuilders.clientapi\ 
│                ├── integration\ 
│                   └── ClientApiIntegrationTest.java
│                ├── repository\ 
│                   └── ClientApiIntegrationTest.java
│          └── resources\
└── 
```

You can run the tests by gradlew as well:
```bash
./gradlew test
```

#### Tools
[Spring Boot Test]: application framework
[JUnit]: test runner
[Hamcrest Matchers]: assertions
[MockMVC]: testing Spring Controllers


---
##Libraries
Third Party Libraries

* [Official Gradle documentation](https://docs.gradle.org)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.3.1.RELEASE/gradle-plugin/reference/html/)
* [Spring Boot](http://projects.spring.io/spring-boot/) - Spring Boot makes it easy to create stand-alone, production-grade Spring based Applications that you can "just run".
* [Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/2.3.1.RELEASE/reference/htmlsingle/#using-boot-devtools)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.3.1.RELEASE/reference/htmlsingle/#boot-features-developing-web-applications)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.3.1.RELEASE/reference/htmlsingle/#boot-features-jpa-and-spring-data)

---

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)

---

### Additional Links
These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)

---
### Future Work :: Point of Improvements/Enhancements
- Provide a more robust security mechanism using JWT Token instead plain Api key
- Provide an API Testing using some framework like Karate / Cucumber
- Provide some Contract Testing using the proper Swagger API